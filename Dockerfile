FROM docker:20.10.11-dind

RUN apk add --no-cache curl bash git
RUN apk add --no-cache python3 py3-pip
RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev cargo make
RUN pip install azure-cli
RUN curl -O https://download.visualstudio.microsoft.com/download/pr/254b4820-7917-4248-b353-a6350020be96/19ceb70a7b825f866761c4e2ae0d6d3f/dotnet-sdk-3.1.415-linux-musl-x64.tar.gz \
    && mkdir -p /usr/bin/dotnet \
    && tar -xzvf dotnet-sdk-3.1.415-linux-musl-x64.tar.gz -C /usr/bin/dotnet \
    # Install kubernetes cli
    && curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl \
    && rm -rf \
        dotnet-sdk-3.1.415-linux-musl-x64.tar.gz \
        /tmp/* \
        /var/cache/apk/*

# Install docker cli
#RUN apk add --no-cache --update docker openrc
#RUN rc-update add docker boot

# Install docker-compose
RUN curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

# Export dotnet command
ENV PATH=$PATH:/usr/bin/dotnet
